package com.example.fastparking.login

import com.example.fastparking.model.User

interface Login {
    interface LoginView {
        fun showUserInfo(user: User)
        fun showError()
    }

    interface LoginPresenter {
        fun getUserInfo(email: String, password: String)
    }
}