package com.example.fastparking.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.fastparking.main.MainActivity
import com.example.fastparking.databinding.ActivityLoginBinding
import com.example.fastparking.login.presenter.LoginPresenter
import com.example.fastparking.model.User

class LoginActivity : AppCompatActivity(), Login.LoginView {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var loginPresenter: LoginPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setupLoginPresenter()
        binding.buttonEnter.setOnClickListener { signIn() }
    }

    private fun setupLoginPresenter() {
        loginPresenter = LoginPresenter(this)
    }

    private fun checkFields(): Boolean {
        val email = binding.emailEditText.text.toString()
        val password = binding.passwordEditText.text.toString()

        if (email.isEmpty() && password.isEmpty()) {
            Toast.makeText(this, "Informe seus dados para continuar.", Toast.LENGTH_SHORT).show()
            return false
        }

        if (email.isEmpty()) {
            Toast.makeText(this, "Informe o seu email.", Toast.LENGTH_SHORT).show()
            return false
        }

        if (password.isEmpty()) {
            Toast.makeText(this, "Informe a sua senha.", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }

    private fun signIn() {
        val email = binding.emailEditText.text.toString()
        val password = binding.passwordEditText.text.toString()
        if (checkFields()) {
            loginPresenter.getUserInfo(email, password)
        }
    }

    override fun showUserInfo(user: User) {

        val intent = Intent(this, MainActivity::class.java).apply {
         putExtra("user", user)
        }
        this.startActivity(intent)
        this.finish()

    }

    override fun showError() {
        Toast.makeText(
            this,
            "Verifique seus dados. Email e/ou senha incorretos.",
            Toast.LENGTH_LONG
        ).show()
    }
}