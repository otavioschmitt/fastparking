package com.example.fastparking.login.presenter

import com.example.fastparking.login.Login
import com.example.fastparking.model.LoginResponse
import com.example.fastparking.network.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter(
    private val loginView: Login.LoginView

) : Login.LoginPresenter {

    override fun getUserInfo(email: String, password: String) {
        RetrofitClient.fastParkingAPI
            .userLogin(email, password)
            .enqueue(object : Callback<LoginResponse> {
                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    if (!response.isSuccessful) {
                        loginView.showError()
                        return
                    }

                    response.body()?.user?.let {
                        loginView.showUserInfo(it)
                    }
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    loginView.showError()
                }
            })
    }
}
