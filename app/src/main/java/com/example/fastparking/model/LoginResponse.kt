package com.example.fastparking.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

data class LoginResponse (
    val user: User?
)

@Parcelize
class User (
    val name: String?,
    val email: String?,
    val birthDate: Date?,
    val cellphone: String?
): Parcelable