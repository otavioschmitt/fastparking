package com.example.fastparking.network


import com.example.fastparking.model.LoginResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface FastParkingAPI {

    @GET("user/auth")
    fun userLogin(
        @Query("email") email: String?,
        @Query("password") password: String?
    ): Call<LoginResponse>

}