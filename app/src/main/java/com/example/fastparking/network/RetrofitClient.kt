package com.example.fastparking.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    private const val FASTPARKING_BASE_URL = "https://api.fastparking.com.br/"

    val fastParkingAPI = Retrofit.Builder()
        .baseUrl(FASTPARKING_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(FastParkingAPI::class.java)
}