package com.example.fastparking.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.fastparking.databinding.ActivityMainBinding
import com.example.fastparking.model.User
import java.text.SimpleDateFormat

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setupInfo()
    }

    private fun setupInfo() {
        val user = getExtras()
        val birthDate = formatData(user)

        binding.welcomeTextView.text = "Bem vindo, ${user.name}"
        binding.usernameTextView.text = "Nome: ${user.name}"
        binding.userEmailTextView.text = "Email: ${user.email}"
        binding.birthdateTextView.text = "Data de nascimento: ${birthDate}"
        binding.phoneTextView.text = "Telefone: ${user.cellphone}"
    }

    private fun getExtras(): User {
        val user = intent.extras?.get("user") as User
        return user
    }

    private fun formatData(user: User): String {
        val format = "dd/MMM/yyyy"
        val simpleDateFormat = SimpleDateFormat(format)
        val birthDate = simpleDateFormat.format(user.birthDate)
        return birthDate
    }
}